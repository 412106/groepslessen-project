﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterfaceLib
{
    public class UserDTO
    {
        public string FirstName;
        public string LastName;
        public string AddressLine1;
        public string AddressLine2;
        public DateTime DateOfBirth;
        public string Email;
        public string Password;
        public string PhoneNumber;
        public long ID;
        public bool IsAdmin;

        public UserDTO(string firstName, string lastName, string addressLine1, string addressLine2, DateTime dateOfBirth, string email, string password, string phoneNumber, long iD, bool isAdmin)
        {
            FirstName = firstName;
            LastName = lastName;
            AddressLine1 = addressLine1;
            AddressLine2 = addressLine2;
            DateOfBirth = dateOfBirth;
            Email = email;
            Password = password;
            PhoneNumber = phoneNumber;
            ID = iD;
            IsAdmin = isAdmin;
        }
    }
}