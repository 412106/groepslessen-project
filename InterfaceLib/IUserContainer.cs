﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterfaceLib
{
    public interface IUserContainer
    {
        public void Create(UserDTO dto);
        public UserDTO FindByID(long id);
        public void Update(UserDTO dto);
        public void Delete(UserDTO dto);
        public UserDTO FindByEmailAndPassword(string email, string password);
        public List<UserDTO> GetAll();
    }
}
