﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterfaceLib
{
    public interface IWorkoutContainer
    {
        public void Create(WorkoutDTO dto);
        public WorkoutDTO FindByID(long id);
        public void Update(WorkoutDTO dto);
        public void Delete(WorkoutDTO dto);
        public List<WorkoutDTO> GetAll();
    }
}
