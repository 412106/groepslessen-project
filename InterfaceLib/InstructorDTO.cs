﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterfaceLib
{
    public class InstructorDTO
    {
        public string Name;
        public long ID;
        public string Specialty;

        public InstructorDTO(string name, long iD, string specialty)
        {
            Name = name;
            ID = iD;
            Specialty = specialty;
        }
    }
}
