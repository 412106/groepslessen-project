﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterfaceLib
{
    public interface IGroupClassContainer
    {
        public void Create(GroupClassDTO dto);
        public GroupClassDTO FindByID(long id);
        public void Update(GroupClassDTO dto);
        public void Delete(GroupClassDTO dto);
        public List<GroupClassDTO> GetAll();
    }
}
