﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterfaceLib
{
    public interface IInstructorContainer
    {
        public void Create(InstructorDTO dto);
        public InstructorDTO? FindByID(long id);
        public void Update(InstructorDTO dto);
        public void Delete(InstructorDTO dto);
        public List<InstructorDTO> GetAll();
    }
}
