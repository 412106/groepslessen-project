﻿namespace InterfaceLib
{
    public class WorkoutDTO
    {
        public string Name;
        public string Description;
        public long ID;
        public string URL;

        public WorkoutDTO(string name, string description, long iD, string uRL)
        {
            Name = name;
            Description = description;
            ID = iD;
            URL = uRL;
        }

        public override bool Equals(object? other)
        {
            if (other != null)
            {
                WorkoutDTO dto = other as WorkoutDTO;
                if (dto.ID == this.ID)
                {
                    return true;
                }
            }
            return false;
        }
    }
}