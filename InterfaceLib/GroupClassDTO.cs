﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterfaceLib
{
    public class GroupClassDTO
    {
        public string Name;
        public long ID;
        public int Duration;
        public DateTime Date;
        public InstructorDTO Instructor;
        public WorkoutDTO Workout;

        public GroupClassDTO(string name, long iD, int duration, DateTime date, InstructorDTO instructor, WorkoutDTO workout)
        {
            Name = name;
            ID = iD;
            Duration = duration;
            Date = date;
            Instructor = instructor;
            Workout = workout;
        }
    }
}
