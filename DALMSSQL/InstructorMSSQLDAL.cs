﻿using InterfaceLib;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UTDAL
{
    public class InstructorMSSQLDAL : IInstructorContainer
    {
        /// <summary>
        /// De connectionstring van de database. Hiermee wordt de verbinding naar de database gemaakt.
        /// </summary>
        private SqlConnection conn = new SqlConnection("Server=mssqlstud.fhict.local;Database=dbi412106;User Id=dbi412106;Password=Martijn2001;");

        /// <summary>
        /// Hiermee wordt een nieuwe instructor aangemaakt.
        /// </summary>
        /// <param name="dto">De instructor die wordt meegegeven</param>
        /// <exception cref="TemporaryException">Error bij verbindingsproblemen met de database</exception>
        /// <exception cref="PermanentException">Error bij fouten in het programma</exception>
        public void Create(InstructorDTO dto)
        {
            try
            {
                SqlCommand command = new SqlCommand("INSERT INTO Instructor VALUES (@name, @specialty)", conn);
                command.Parameters.AddWithValue("@name", dto.Name);
                command.Parameters.AddWithValue("@specialty", dto.Specialty);
                ExecuteNonQuery(command, conn);
            }
            catch (SqlException)
            {
                throw new TemporaryException("Cannot connect to the server. Please try again later.");
            }
            catch (Exception ex)
            {
                throw new PermanentException("Could not fulfill your request at this time. Reason: " + ex.Message);
            }
        }

        /// <summary>
        /// Hiermee wordt de instructor die wordt meegegeven verwijderd.
        /// </summary>
        /// <param name="dto">De instructor die wordt meegegeven</param>
        /// <exception cref="TemporaryException">Error bij verbindingsproblemen met de database</exception>
        /// <exception cref="PermanentException">Error bij fouten in het programma</exception>
        public void Delete(InstructorDTO dto)
        {
            try
            {
                SqlCommand command = new SqlCommand("DELETE FROM Instructor WHERE ID = @id", conn);
                command.Parameters.AddWithValue("@id", dto.ID);
                ExecuteNonQuery(command, conn);
            }
            catch (SqlException)
            {
                throw new TemporaryException("Cannot connect to the server. Please try again later.");
            }
            catch (Exception ex)
            {
                throw new PermanentException("Could not fulfill your request at this time. Reason: " + ex.Message);
            }
        }

        /// <summary>
        /// Hiermee kun je alle informatie over een bepaalde instructor vinden met het ID van de instructor.
        /// </summary>
        /// <param name="id">Het ID van de instructor</param>
        /// <returns>Een instructor waarvan het ID het meegegeven ID matched</returns>
        /// <exception cref="TemporaryException">Error bij verbindingsproblemen met de database</exception>
        /// <exception cref="PermanentException">Error bij fouten in het programma</exception>
        public InstructorDTO? FindByID(long id)
        {
            try
            {
                SqlCommand command = new SqlCommand("SELECT * FROM Instructor WHERE ID = @id", conn);
                command.Parameters.AddWithValue("@id", id);
                conn.Open();
                SqlDataReader reader = command.ExecuteReader();
                List<InstructorDTO> instructor = ReadInstructors(reader);
                if (instructor.Count == 0)
                {
                    conn.Close();
                    return null;
                }
                else
                {
                    conn.Close();
                    return instructor[0];
                }
            }
            catch (SqlException)
            {
                throw new TemporaryException("Cannot connect to the server. Please try again later.");
            }
            catch (Exception ex)
            {
                throw new PermanentException("Could not fulfill your request at this time. Reason: " + ex.Message);
            }
        }

        /// <summary>
        /// Hiermee vraag je alle instructors en de bijbehorende informatie die in de database staan op
        /// </summary>
        /// <returns>Een lijst met alle instructors</returns>
        /// <exception cref="TemporaryException">Error bij verbindingsproblemen met de database</exception>
        /// <exception cref="PermanentException">Error bij fouten in het programma</exception>
        public List<InstructorDTO> GetAll()
        {
            try
            {
                List<InstructorDTO> allInstructors = new List<InstructorDTO>();
                SqlCommand command = new SqlCommand("SELECT * FROM Instructor", conn);
                conn.Open();
                SqlDataReader reader = command.ExecuteReader();
                allInstructors = ReadInstructors(reader);
                conn.Close();
                return allInstructors;
            }
            catch (SqlException)
            {
                throw new TemporaryException("Cannot connect to the server. Please try again later.");
            }
            catch (Exception ex)
            {
                throw new PermanentException("Could not fulfill your request at this time. Reason: " + ex.Message);
            }
        }

        /// <summary>
        /// Maakt van de informatie in de database een nieuwe instructor aan en voegt deze toe aan een lijst
        /// </summary>
        /// <param name="reader">Een instantie van de SqlDataReader</param>
        /// <returns>Een lijst met een of meerdere instructors</returns>
        /// <exception cref="TemporaryException">Error bij verbindingsproblemen met de database</exception>
        /// <exception cref="PermanentException">Error bij fouten in het programma</exception>
        private List<InstructorDTO> ReadInstructors(SqlDataReader reader)
        {
            try
            {
                List<InstructorDTO> allInstructors = new();
                while (reader.Read())
                {
                    allInstructors.Add(new InstructorDTO(reader["InstrName"].ToString(), Convert.ToInt64(reader["ID"]),
                        reader["Specialty"].ToString()));
                }
                return allInstructors;
            }
            catch (SqlException)
            {
                throw new TemporaryException("Cannot connect to the server. Please try again later.");
            }
            catch (Exception ex)
            {
                throw new PermanentException("Could not fulfill your request at this time. Reason: " + ex.Message);
            }
        }

        /// <summary>
        /// Update de gegevens in de database voor de meegegeven instructor
        /// </summary>
        /// <param name="dto">De instructor die wordt meegegeven</param>
        /// <exception cref="TemporaryException">Error bij verbindingsproblemen met de database</exception>
        /// <exception cref="PermanentException">Error bij fouten in het programma</exception>
        public void Update(InstructorDTO dto)
        {
            try
            {
                SqlCommand command = new SqlCommand("UPDATE Instructor SET InstrName = @name, Specialty = @specialty WHERE ID = @id", conn);
                command.Parameters.AddWithValue("@name", dto.Name);
                command.Parameters.AddWithValue("@specialty", dto.Specialty);
                command.Parameters.AddWithValue("@id", dto.ID);
                ExecuteNonQuery(command, conn);
            }
            catch (SqlException)
            {
                throw new TemporaryException("Cannot connect to the server. Please try again later.");
            }
            catch (Exception ex)
            {
                throw new PermanentException("Could not fulfill your request at this time. Reason: " + ex.Message);
            }
        }

        /// <summary>
        /// Opent de database connectie, voert de query uit, en sluit de connectie.
        /// </summary>
        /// <param name="command">Instantie van SqlCommand</param>
        /// <param name="conn">Instantie van SqlConnection</param>
        public void ExecuteNonQuery(SqlCommand command, SqlConnection conn)
        {
            conn.Open();
            command.ExecuteNonQuery();
            conn.Close();
        }
    }
}
