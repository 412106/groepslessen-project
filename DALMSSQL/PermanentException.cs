﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace UTDAL
{
    public class PermanentException : Exception
    {
        public PermanentException()
        {
        }

        public PermanentException(string? message) : base(message)
        {
        }

        public PermanentException(string? message, Exception? innerException) : base(message, innerException)
        {
        }

        protected PermanentException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
