﻿using InterfaceLib;
using System.Data.SqlClient;
using System.Data;

namespace UTDAL
{
    public class UserMSSQLDAL : IUserContainer
    {
        /// <summary>
        /// De connectionstring van de database. Hiermee wordt de verbinding naar de database gemaakt.
        /// </summary>
        private SqlConnection conn = new SqlConnection("Server=mssqlstud.fhict.local;Database=dbi412106;User Id=dbi412106;Password=Martijn2001;");

        /// <summary>
        /// Hiermee wordt een nieuwe user aangemaakt.
        /// </summary>
        /// <param name="dto">De user die wordt meegegeven</param>
        /// <exception cref="TemporaryException">Error bij verbindingsproblemen met de database</exception>
        /// <exception cref="PermanentException">Error bij fouten in het programma</exception>
        public void Create(UserDTO dto)
        {
            try
            {
                dto.Password = BCrypt.Net.BCrypt.EnhancedHashPassword(dto.Password, 13);
                SqlCommand command = new SqlCommand(@"INSERT INTO [User] VALUES (@name,@lastname, @address1, @address2, @date, @email,
                @hash, @phone, @isAdmin)", conn);
                command.Parameters.AddWithValue("@name", dto.FirstName);
                command.Parameters.AddWithValue("@lastname", dto.LastName);
                command.Parameters.AddWithValue("@address1", dto.AddressLine1);
                command.Parameters.AddWithValue("@address2", dto.AddressLine2);
                command.Parameters.AddWithValue("@date", dto.DateOfBirth);
                command.Parameters.AddWithValue("@email", dto.Email);
                command.Parameters.AddWithValue("@hash", dto.Password);
                command.Parameters.AddWithValue("@phone", dto.PhoneNumber);
                command.Parameters.AddWithValue("@isAdmin", dto.IsAdmin);
                ExecuteNonQuery(command, conn);
            }
            catch (SqlException)
            {
                throw new TemporaryException("Cannot connect to the server. Please try again later.");
            }
            catch (Exception ex)
            {
                throw new PermanentException("Could not fulfill your request at this time. Reason: " + ex.Message);
            }
        }

        /// <summary>
        /// Hiermee wordt de user die wordt meegeven verwijderd.
        /// </summary>
        /// <param name="dto">De user die wordt meegegeven</param>
        /// <exception cref="TemporaryException">Error bij verbindingsproblemen met de database</exception>
        /// <exception cref="PermanentException">Error bij fouten in het programma</exception>
        public void Delete(UserDTO dto)
        {
            try
            {
                SqlCommand command = new SqlCommand("DELETE FROM [User] WHERE UserID = @id", conn);
                command.Parameters.AddWithValue("@id", dto.ID);
                ExecuteNonQuery(command, conn);
            }
            catch (SqlException ex)
            {
                throw new TemporaryException("Cannot connect to the server. Please try again later." + ex.Message);
            }
            catch (Exception ex)
            {
                throw new PermanentException("Could not fulfill your request at this time. Reason: " + ex.Message);
            }
        }

        /// <summary>
        /// Hiermee wordt gekeken of er een match is in de database voor de email en het wachtwoord
        /// die de user invult bij het inloggen.
        /// </summary>
        /// <param name="email">De email die de user invult bij het inloggen</param>
        /// <param name="password">Het wachtwoord die de user invult bij het inloggen</param>
        /// <returns>De user die bij het email en wachtwoord hoort. Als er geen match is, dan null</returns>
        /// <exception cref="TemporaryException">Error bij verbindingsproblemen met de database</exception>
        /// <exception cref="PermanentException">Error bij fouten in het programma</exception>
        public UserDTO? FindByEmailAndPassword(string email, string password)
        {
            try
            {
                UserDTO? dto = null;
                SqlCommand command = new SqlCommand("SELECT * FROM [User] WHERE Email = @email", conn);
                command.Parameters.AddWithValue("@email", email);
                conn.Open();
                SqlDataReader reader = command.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        string hash = reader["PasswordHash"].ToString();
                        bool correct = BCrypt.Net.BCrypt.EnhancedVerify(password, hash);
                        if (correct)
                        {
                            dto = new UserDTO(reader["FirstName"].ToString(), reader["LastName"].ToString(),
                                reader["AddressLine1"].ToString(), reader["AddressLine2"].ToString(),
                                Convert.ToDateTime(reader["DateOfBirth"]), reader["Email"].ToString(), reader["PasswordHash"].ToString(),
                                reader["PhoneNumber"].ToString(), Convert.ToInt64(reader["UserID"]), Convert.ToBoolean(reader["IsAdmin"]));
                        }
                    }

                    conn.Close();
                }
                return dto;
            }
            catch (SqlException)
            {
                throw new TemporaryException("Cannot connect to the server. Please try again later.");
            }
            catch (Exception ex)
            {
                throw new PermanentException("Could not fulfill your request at this time. Reason: " + ex.Message);
            }
        }

        /// <summary>
        /// Hiermee kun je alle informatie over een bepaalde user vinden met het ID van de user.
        /// </summary>
        /// <param name="id">Het ID van de user</param>
        /// <returns>Een user waarvan het ID het meegegeven ID matched</returns>
        /// <exception cref="TemporaryException">Error bij verbindingsproblemen met de database</exception>
        /// <exception cref="PermanentException">Error bij fouten in het programma</exception>
        public UserDTO FindByID(long id)
        {
            try
            {
                SqlCommand command = new SqlCommand("SELECT * FROM [User] WHERE UserID = @id", conn);
                command.Parameters.AddWithValue("@id", id);
                conn.Open();
                SqlDataReader reader = command.ExecuteReader();
                List<UserDTO> user = ReadUsers(reader);
                if (user.Count == 0)
                {
                    conn.Close();
                    return null;
                }
                else
                {
                    conn.Close();
                    return user[0];
                }
            }
            catch (SqlException)
            {
                throw new TemporaryException("Cannot connect to the server. Please try again later.");
            }
            catch (Exception ex)
            {
                throw new PermanentException("Could not fulfill your request at this time. Reason: " + ex.Message);
            }
        }

        /// <summary>
        /// Hiermee vraag je alle users en de bijbehorende informatie die in de database staan op
        /// </summary>
        /// <returns>Een lijst met alle users</returns>
        /// <exception cref="TemporaryException">Error bij verbindingsproblemen met de database</exception>
        /// <exception cref="PermanentException">Error bij fouten in het programma</exception>
        public List<UserDTO> GetAll()
        {
            try
            {
                List<UserDTO> allUsers = new List<UserDTO>();
                SqlCommand command = new SqlCommand(@"SELECT * FROM [User]", conn);
                conn.Open();
                SqlDataReader reader = command.ExecuteReader();
                allUsers = ReadUsers(reader);
                conn.Close();
                return allUsers;
            }
            catch (SqlException)
            {
                throw new TemporaryException("Cannot connect to the server. Please try again later.");
            }
            catch (Exception ex)
            {
                throw new PermanentException("Could not fulfill your request at this time. Reason: " + ex.Message);
            }
        }

        /// <summary>
        /// Maakt van de informatie in de database een nieuwe user aan en voegt deze toe aan een lijst
        /// </summary>
        /// <param name="reader">Een instantie van de SqlDataReader</param>
        /// <returns>Een lijst met een of meerdere users</returns>
        /// <exception cref="TemporaryException">Error bij verbindingsproblemen met de database</exception>
        /// <exception cref="PermanentException">Error bij fouten in het programma</exception>
        private List<UserDTO> ReadUsers(SqlDataReader reader)
        {
            try
            {
                List<UserDTO> allUsers = new();
                while (reader.Read())
                {
                    allUsers.Add(new UserDTO(reader["FirstName"].ToString(), reader["LastName"].ToString(),
                                reader["AddressLine1"].ToString(), reader["AddressLine2"].ToString(),
                                Convert.ToDateTime(reader["DateOfBirth"]), reader["Email"].ToString(), reader["PasswordHash"].ToString(),
                                reader["PhoneNumber"].ToString(), Convert.ToInt64(reader["UserID"]), Convert.ToBoolean(reader["IsAdmin"])));
                }
                return allUsers;
            }
            catch (SqlException)
            {
                throw new TemporaryException("Cannot connect to the server. Please try again later.");
            }
            catch (Exception ex)
            {
                throw new PermanentException("Could not fulfill your request at this time. Reason: " + ex.Message);
            }
        }

        /// <summary>
        /// Update de gegevens in de database voor de meegegeven instructor
        /// </summary>
        /// <param name="dto">De instructor die wordt meegegeven</param>
        /// <exception cref="TemporaryException">Error bij verbindingsproblemen met de database</exception>
        /// <exception cref="PermanentException">Error bij fouten in het programma</exception>
        public void Update(UserDTO dto)
        {
            try
            {
                dto.Password = BCrypt.Net.BCrypt.EnhancedHashPassword(dto.Password, 13);
                SqlCommand command = new SqlCommand("UPDATE [User] SET FirstName = @name, LastName = @lastname, AddressLine1 = @address1," +
                    "AddressLine2 = @address2, Email = @email, PasswordHash = @hash, PhoneNumber = @phone WHERE UserID = @id", conn);
                command.Parameters.AddWithValue("@name", dto.FirstName);
                command.Parameters.AddWithValue("@lastname", dto.LastName);
                command.Parameters.AddWithValue("@address1", dto.AddressLine1);
                command.Parameters.AddWithValue("@address2", dto.AddressLine2);
                command.Parameters.AddWithValue("@date", dto.DateOfBirth);
                command.Parameters.AddWithValue("@email", dto.Email);
                command.Parameters.AddWithValue("@hash", dto.Password);
                command.Parameters.AddWithValue("@phone", dto.PhoneNumber);
                command.Parameters.AddWithValue("@ID", dto.ID);
                ExecuteNonQuery(command, conn);
            }
            catch (SqlException)
            {
                throw new TemporaryException("Cannot connect to the server. Please try again later.");
            }
            catch (Exception ex)
            {
                throw new PermanentException("Could not fulfill your request at this time. Reason: " + ex.Message);
            }
        }

        /// <summary>
        /// Opent de database connectie, voert de query uit, en sluit de connectie.
        /// </summary>
        /// <param name="command">Instantie van SqlCommand</param>
        /// <param name="conn">Instantie van SqlConnection</param>
        public void ExecuteNonQuery(SqlCommand command, SqlConnection conn)
        {
            conn.Open();
            command.ExecuteNonQuery();
            conn.Close();
        }
    }
}