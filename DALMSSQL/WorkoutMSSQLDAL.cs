﻿using InterfaceLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;

namespace UTDAL
{
    public class WorkoutMSSQLDAL : IWorkoutContainer
    {
        /// <summary>
        /// De connectionstring van de database. Hiermee wordt de verbinding naar de database gemaakt.
        /// </summary>
        private SqlConnection conn = new SqlConnection("Server=mssqlstud.fhict.local;Database=dbi412106;User Id=dbi412106;Password=Martijn2001;");

        /// <summary>
        /// Hiermee wordt een nieuwe workout aangemaakt.
        /// </summary>
        /// <param name="dto">De workout die wordt meegegeven</param>
        /// <exception cref="TemporaryException">Error bij verbindingsproblemen met de database</exception>
        /// <exception cref="PermanentException">Error bij fouten in het programma</exception>
        public void Create(WorkoutDTO dto)
        {
            try
            {
                SqlCommand command = new SqlCommand("INSERT INTO Workout VALUES (@name, @description, @url)", conn);
                command.Parameters.AddWithValue("@name", dto.Name);
                command.Parameters.AddWithValue("@description", dto.Description);
                command.Parameters.AddWithValue("@url", dto.URL);
                ExecuteNonQuery(command, conn);
            }
            catch (SqlException)
            {
                throw new TemporaryException("Cannot connect to the server. Please try again later.");
            }
            catch (Exception ex)
            {
                throw new PermanentException("Could not fulfill your request at this time. Reason: " + ex.Message);
            }
        }

        /// <summary>
        /// Hiermee wordt de workout die wordt meegegeven verwijderd.
        /// </summary>
        /// <param name="dto">De workout die wordt meegegeven</param>
        /// <exception cref="TemporaryException">Error bij verbindingsproblemen met de database</exception>
        /// <exception cref="PermanentException">Error bij fouten in het programma</exception>
        public void Delete(WorkoutDTO dto)
        {
            try
            {
                SqlCommand command = new SqlCommand("DELETE FROM Workout WHERE ID = @id", conn);
                command.Parameters.AddWithValue("@id", dto.ID);
                ExecuteNonQuery(command, conn);
            }
            catch (SqlException)
            {
                throw new TemporaryException("Cannot connect to the server. Please try again later.");
            }
            catch (Exception ex)
            {
                throw new PermanentException("Could not fulfill your request at this time. Reason: " + ex.Message);
            }
        }

        /// <summary>
        /// Hiermee kun je alle informatie over een bepaalde workout vinden met het ID van de workout.
        /// </summary>
        /// <param name="id">Het ID van de workout</param>
        /// <returns>Een workout waarvan het ID het meegegeven ID matched</returns>
        /// <exception cref="TemporaryException">Error bij verbindingsproblemen met de database</exception>
        /// <exception cref="PermanentException">Error bij fouten in het programma</exception>
        public WorkoutDTO FindByID(long id)
        {
            try
            {
                SqlCommand command = new SqlCommand("SELECT * FROM Workout WHERE ID = @id", conn);
                command.Parameters.AddWithValue("@id", id);
                conn.Open();
                SqlDataReader reader = command.ExecuteReader();
                List<WorkoutDTO> workout = ReadWorkouts(reader);
                if(workout.Count == 0)
                {
                    conn.Close();
                    return null;
                }
                else
                {
                    conn.Close();
                    return workout[0];
                }
            }
            catch (SqlException)
            {
                throw new TemporaryException("Cannot connect to the server. Please try again later.");
            }
            catch (Exception ex)
            {
                throw new PermanentException("Could not fulfill your request at this time. Reason: " + ex.Message);
            }
        }

        /// <summary>
        /// Hiermee vraag je alle workouts en de bijbehorende informatie die in de database staan op
        /// </summary>
        /// <returns>Een lijst met alle workouts</returns>
        /// <exception cref="TemporaryException">Error bij verbindingsproblemen met de database</exception>
        /// <exception cref="PermanentException">Error bij fouten in het programma</exception>
        public List<WorkoutDTO> GetAll()
        {
            try
            {
                List<WorkoutDTO> allWorkouts = new List<WorkoutDTO>();
                SqlCommand command = new SqlCommand("SELECT * FROM Workout", conn);
                conn.Open();
                SqlDataReader reader = command.ExecuteReader();
                allWorkouts = ReadWorkouts(reader);
                conn.Close();
                return allWorkouts;
            }
            catch (SqlException)
            {
                throw new TemporaryException("Cannot connect to the server. Please try again later.");
            }
            catch (Exception ex)
            {
                throw new PermanentException("Could not fulfill your request at this time. Reason: " + ex.Message);
            }
        }

        /// <summary>
        /// Maakt van de informatie in de database een nieuwe workout aan en voegt deze toe aan een lijst.
        /// </summary>
        /// <param name="reader">Een instantie van de SqlDataReader</param>
        /// <returns>Een lijst met een of meerdere workouts</returns>
        /// <exception cref="TemporaryException">Error bij verbindingsproblemen met de database</exception>
        /// <exception cref="PermanentException">Error bij fouten in het programma</exception>
        public List<WorkoutDTO> ReadWorkouts(SqlDataReader reader)
        {
            List<WorkoutDTO> allWorkouts = new List<WorkoutDTO>();
            while (reader.Read())
            {
                allWorkouts.Add(new WorkoutDTO(reader["WorkoutName"].ToString(), reader["Description"].ToString(),
                    Convert.ToInt64(reader["ID"]), reader["URL"].ToString()));
            }
            return allWorkouts;
        }

        /// <summary>
        /// Update de gegevens in de database voor de meegegeven workout.
        /// </summary>
        /// <param name="dto">De workout die wordt meegegeven</param>
        /// <exception cref="TemporaryException">Error bij verbindingsproblemen met de database</exception>
        /// <exception cref="PermanentException">Error bij fouten in het programma</exception>
        public void Update(WorkoutDTO dto)
        {
            try
            {
                SqlCommand command = new SqlCommand("UPDATE Workout SET WorkoutName = @name, Description = @description, URL = @url WHERE ID = @id", conn);
                command.Parameters.AddWithValue("@name", dto.Name);
                command.Parameters.AddWithValue("@description", dto.Description);
                command.Parameters.AddWithValue("@id", dto.ID);
                command.Parameters.AddWithValue("@url", dto.URL);
                ExecuteNonQuery(command, conn);
            }
            catch (SqlException)
            {
                throw new TemporaryException("Cannot connect to the server. Please try again later.");
            }
            catch (Exception ex)
            {
                throw new PermanentException("Could not fulfill your request at this time. Reason: " + ex.Message);
            }
        }

        /// <summary>
        /// Opent de database connectie, voert de query uit, en sluit de connectie.
        /// </summary>
        /// <param name="command">Instantie van SqlCommand</param>
        /// <param name="conn">Instantie van SqlConnection</param>
        public void ExecuteNonQuery(SqlCommand command, SqlConnection conn)
        {
            conn.Open();
            command.ExecuteNonQuery();
            conn.Close();
        }
    }
}
