﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace UTDAL
{
    public class TemporaryException : Exception
    {

        public TemporaryException(string? message) : base(message)
        {
        }

        public TemporaryException(string? message, Exception? innerException) : base(message, innerException)
        {
        }

        protected TemporaryException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
