﻿using InterfaceLib;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UTDAL
{
    public class GroupClassMSSQLDAL : IGroupClassContainer
    {
        /// <summary>
        /// De connectionstring van de database. Hiermee wordt de verbinding naar de database gemaakt.
        /// </summary>
        private SqlConnection conn = new SqlConnection("Server=mssqlstud.fhict.local;Database=dbi412106;User Id=dbi412106;Password=Martijn2001;");

        /// <summary>
        /// Hiermee wordt een nieuwe groupclass aangemaakt.
        /// </summary>
        /// <param name="dto">De groupclass die wordt meegegeven</param>
        /// <exception cref="TemporaryException">Error bij verbindingsproblemen met de database</exception>
        /// <exception cref="PermanentException">Error bij fouten in het programma</exception>
        public void Create(GroupClassDTO dto)
        {
            try
            {
                SqlCommand command = new SqlCommand("INSERT INTO GroupClass VALUES (@name, @duration, @date, @instructor, @workout)", conn);
                command.Parameters.AddWithValue("@name", dto.Name);
                command.Parameters.AddWithValue("@duration", dto.Duration);
                command.Parameters.AddWithValue("@date", dto.Date);
                command.Parameters.AddWithValue("@instructor", dto.Instructor.ID);
                command.Parameters.AddWithValue("@workout", dto.Workout.ID);
                ExecuteNonQuery(command, conn);
            }
            catch (SqlException)
            {
                throw new TemporaryException("Cannot connect to the server. Please try again later.");
            }
            catch (Exception ex)
            {
                throw new PermanentException("Could not fulfill your request at this time. Reason: " + ex.Message);
            }
        }

        /// <summary>
        /// Hiermee wordt de groupclass die wordt meegegeven verwijderd.
        /// </summary>
        /// <param name="dto">De groupclass die wordt meegegeven</param>
        /// <exception cref="TemporaryException">Error bij verbindingsproblemen met de database</exception>
        /// <exception cref="PermanentException">Error bij fouten in het programma</exception>
        public void Delete(GroupClassDTO dto)
        {
            try
            {
                SqlCommand command = new SqlCommand("DELETE FROM GroupClass WHERE ClassID = @id", conn);
                command.Parameters.AddWithValue("@id", dto.ID);
                ExecuteNonQuery(command, conn);
            }
            catch (SqlException)
            {
                throw new TemporaryException("Cannot connect to the server. Please try again later.");
            }
            catch (Exception ex)
            {
                throw new PermanentException("Could not fulfill your request at this time. Reason: " + ex.Message);
            }
        }

        /// <summary>
        /// Hiermee kun je alle informatie over een bepaalde groupclass vinden met het ID van de groupclass.
        /// </summary>
        /// <param name="id">Het ID van de groupclass</param>
        /// <returns>Een groupclass waarvan het ID het meegegeven ID matched</returns>
        /// <exception cref="TemporaryException">Error bij verbindingsproblemen met de database</exception>
        /// <exception cref="PermanentException">Error bij fouten in het programma</exception>
        public GroupClassDTO FindByID(long id)
        {
            try
            {
                SqlCommand command = new SqlCommand(@"SELECT * FROM GroupClass AS G INNER JOIN Instructor AS I ON I.ID = G.InstructorID INNER JOIN Workout AS W ON W.ID = G.WorkoutID WHERE ClassID = @id", conn);
                command.Parameters.AddWithValue("@id", id);
                conn.Open();
                SqlDataReader reader = command.ExecuteReader();
                List<GroupClassDTO> groupClass = ReadGroupClasses(reader);
                if (groupClass.Count == 0)
                {
                    conn.Close();
                    return null;
                }
                else
                {
                    conn.Close();
                    return groupClass[0];
                }
            }
            catch (SqlException)
            {
                throw new TemporaryException("Cannot connect to the server. Please try again later.");
            }
            catch (Exception ex)
            {
                throw new PermanentException("Could not fulfill your request at this time. Reason: " + ex.Message);
            }
        }

        /// <summary>
        /// Hiermee vraag je alle groupclasses en de bijbehorende informatie die in de database staan op
        /// </summary>
        /// <returns>Een lijst met alle groupclasses</returns>
        /// <exception cref="TemporaryException">Error bij verbindingsproblemen met de database</exception>
        /// <exception cref="PermanentException">Error bij fouten in het programma</exception>
        public List<GroupClassDTO> GetAll()
        {
            try
            {
                List<GroupClassDTO> allGroupClasses;
                SqlCommand command = new SqlCommand(@"SELECT * FROM GroupClass AS G INNER JOIN Instructor AS I ON I.ID = G.InstructorID INNER JOIN Workout AS W ON W.ID = G.WorkoutID", conn);
                conn.Open();
                SqlDataReader reader = command.ExecuteReader();
                allGroupClasses = ReadGroupClasses(reader);
                conn.Close();
                return allGroupClasses;
            }
            catch (SqlException)
            {
                throw new TemporaryException("Cannot connect to the server. Please try again later.");
            }
            catch (Exception ex)
            {
                throw new PermanentException("Could not fulfill your request at this time. Reason: " + ex.Message);
            }
        }

        /// <summary>
        /// Maakt van de informatie in de database een nieuwe groupclass aan en voegt deze toe aan een lijst
        /// </summary>
        /// <param name="reader">Een instantie van de SqlDataReader</param>
        /// <returns>Een lijst met een of meerdere groupclasses</returns>
        /// <exception cref="TemporaryException">Error bij verbindingsproblemen met de database</exception>
        /// <exception cref="PermanentException">Error bij fouten in het programma</exception>
        private List<GroupClassDTO> ReadGroupClasses(SqlDataReader reader)
        {
            try
            {
                List<GroupClassDTO> allGroupClasses = new();
                while (reader.Read())
                {
                    allGroupClasses.Add(new GroupClassDTO(reader["Name"].ToString(), Convert.ToInt64(reader["ClassID"]),
                        Convert.ToInt32(reader["Duration"]), Convert.ToDateTime(reader["Date"]),
                        new InstructorDTO(reader["InstrName"].ToString(), Convert.ToInt32(reader["InstructorID"]),
                        reader["Specialty"].ToString()), new WorkoutDTO(reader["WorkoutName"].ToString(), reader["Description"].ToString(), Convert.ToInt64(reader["WorkoutID"]), reader["URL"].ToString())));
                }
                return allGroupClasses;
            }
            catch (SqlException)
            {
                throw new TemporaryException("Cannot connect to the server. Please try again later.");
            }
            catch (Exception ex)
            {
                throw new PermanentException("Could not fulfill your request at this time. Reason: " + ex.Message);
            }
        }

        /// <summary>
        /// Update de gegevens in de database voor de meegegeven groupclass
        /// </summary>
        /// <param name="dto">De groupclass die wordt meegegeven</param>
        /// <exception cref="TemporaryException">Error bij verbindingsproblemen met de database</exception>
        /// <exception cref="PermanentException">Error bij fouten in het programma</exception>
        public void Update(GroupClassDTO dto)
        {
            try
            {
                SqlCommand command = new SqlCommand("UPDATE GroupClass SET Name = @name, Duration = @duration, Date = @date, InstructorID = @instructor, WorkoutID = @workout WHERE ClassID = @id", conn);
                command.Parameters.AddWithValue("@name", dto.Name);
                command.Parameters.AddWithValue("@duration", dto.Duration);
                command.Parameters.AddWithValue("@date", dto.Date);
                command.Parameters.AddWithValue("@id", dto.ID);
                command.Parameters.AddWithValue("@instructor", dto.Instructor.ID);
                command.Parameters.AddWithValue("@workout", dto.Workout.ID);
                ExecuteNonQuery(command, conn);
            }
            catch (SqlException)
            {
                throw new TemporaryException("Cannot connect to the server. Please try again later.");
            }
            catch (Exception ex)
            {
                throw new PermanentException("Could not fulfill your request at this time. Reason: " + ex.Message);
            }
        }

        /// <summary>
        /// Opent de database connectie, voert de query uit, en sluit de connectie.
        /// </summary>
        /// <param name="command">Instantie van SqlCommand</param>
        /// <param name="conn">Instantie van SqlConnection</param>
        public void ExecuteNonQuery(SqlCommand command, SqlConnection conn)
        {
            conn.Open();
            command.ExecuteNonQuery();
            conn.Close();
        }
    }
}

