using BusnLogicGL;
using DALMemoryStore;
using UTDAL;
using System.Data.SqlClient;

namespace Semester2Groepslessen
{
    public partial class Form1 : Form
    {
       public UserContainer uc = new UserContainer(new UserMSSQLDAL());
       public InstructorContainer ic = new InstructorContainer(new InstructorMSSQLDAL());
        public GroupClassContainer gc = new GroupClassContainer(new GroupClassMSSQLDAL());
        public Form1()
        {
            InitializeComponent();
        }

        User u;
        private void button1_Click(object sender, EventArgs e)
        {
            UserContainer uc = new UserContainer(new UserMSSQLDAL());
            u = new User(textBox1.Text, textBox2.Text, textBox3.Text, textBox4.Text, DateTime.Now, textBox6.Text, textBox7.Text, textBox8.Text, 0, false);
           uc.Update(u);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            //g = new GroupClass(textBox1.Text, 2, 3, DateTime.Now, new Instructor("Mako", 2, "Text"));
            //gc.Update(g);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            //g = new GroupClass(textBox1.Text, 1, 3, DateTime.Now, new Instructor("Mako", 2, "Text"));
            //gc.Update(g);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            WorkoutContainer gc = new WorkoutContainer(new WorkoutMSSQLDAL());
            Workout g = new Workout(textBox1.Text, "m", 0, "m");
            gc.Create(g);
        }

        private void button5_Click(object sender, EventArgs e)
        {
           
        }
    }
}