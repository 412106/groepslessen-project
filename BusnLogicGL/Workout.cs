﻿using InterfaceLib;

namespace BusnLogicGL
{
    public class Workout
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public long ID { get; set; }
        public string URL { get; set; }

        /// <summary>
        /// Dit is een workout. Dit geeft aan wat de groepsles inhoudt. Een groepsles heeft altijd een type workout.
        /// </summary>
        /// <param name="name">Naam van de workout</param>
        /// <param name="description">Beschrijving van de workout</param>
        /// <param name="iD">Database ID van de workout</param>
        /// <param name="uRL">Link naar afbeelding van de workout</param>
        public Workout(string name, string description, long iD, string uRL)
        {
            Name = name;
            Description = description;
            ID = iD;
            URL = uRL;
        }

        /// <summary>
        /// In deze constructor wordt DTO omgezet naar domeinklasse
        /// </summary>
        /// <param name="dto">De workout DTO die wordt omgezet</param>
        public Workout(WorkoutDTO dto)
        {
            Name = dto.Name;
            Description = dto.Description;
            ID = dto.ID;
            URL = dto.URL;
        }

        /// <summary>
        /// Hier wordt de domeinklasse omgezet naar DTO
        /// </summary>
        /// <returns>Een workout DTO</returns>
        public WorkoutDTO GetDTO()
        {
            WorkoutDTO dto = new WorkoutDTO(Name, Description, ID, URL);
            return dto;
        }
    }
}