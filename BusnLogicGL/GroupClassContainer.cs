﻿using InterfaceLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusnLogicGL
{
    public class GroupClassContainer
    {
        private readonly IGroupClassContainer DALLaagReferentie;

        public GroupClassContainer(IGroupClassContainer dALLaagReferentie)
        {
            this.DALLaagReferentie = dALLaagReferentie;
        }

        public void Create(GroupClass g)
        {
            GroupClassDTO dto = g.GetDTO();
            DALLaagReferentie.Create(dto);
        }

        public GroupClass FindByID(long ID)
        {
            GroupClassDTO dto = DALLaagReferentie.FindByID(ID);
            return new GroupClass(dto);
        }

        public void Update(GroupClass g)
        {
            GroupClassDTO dto = g.GetDTO();
            DALLaagReferentie.Update(dto);
        }

        public void Delete(GroupClass g)
        {
            GroupClassDTO dto = g.GetDTO();
            DALLaagReferentie.Delete(dto);
        }

        public List<GroupClass> GetAll()
        {
            List<GroupClassDTO> dtos = DALLaagReferentie.GetAll();
            List<GroupClass> groupclasses = new List<GroupClass>();
            foreach (GroupClassDTO dto in dtos)
            {
                groupclasses.Add(new GroupClass(dto));
            }
            return groupclasses;
        }
    }
}

