﻿using InterfaceLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusnLogicGL
{
    public class UserContainer
    {
        private readonly IUserContainer DALLaagReferentie;

        public UserContainer(IUserContainer dALLaagReferentie)
        {
            this.DALLaagReferentie = dALLaagReferentie;
        }

        public void Create(User u)
        {
            UserDTO dto = u.GetDTO();
            DALLaagReferentie.Create(dto);
        }

        public User FindByEmailAndPassword(string email, string password)
        {
            UserDTO dto = DALLaagReferentie.FindByEmailAndPassword(email, password);
            if (dto == null)
            {
                return null;
            }
            else
            {
                return new User(dto);
            }
        }

        public User FindByID(long ID)
        {
            UserDTO dto = DALLaagReferentie.FindByID(ID);
            if (dto == null)
            {
                return null;
            }
            else
            {
                return new User(dto);
            }
        }

        public void Update(User u)
        {
            UserDTO dto = u.GetDTO();
            DALLaagReferentie.Update(dto);
        }

        public void Delete(User u)
        {
            UserDTO dto = u.GetDTO();
            DALLaagReferentie.Delete(dto);
        }

        public List<User> GetAll()
        {
            List<UserDTO> dtos = DALLaagReferentie.GetAll();
            List<User> users = new List<User>();
            foreach (UserDTO dto in dtos)
            {
                users.Add(new User(dto));
            }
            return users;
        }
    }
}
