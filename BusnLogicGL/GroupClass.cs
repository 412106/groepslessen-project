﻿using InterfaceLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks; 

namespace BusnLogicGL
{
    public class GroupClass
    {
        public string Name { get; set; }
        public long ID { get; }
        public int Duration { get; set; }
        public DateTime Date { get; set; }
        public Instructor Instructor { get; set; }
        public Workout Workout { get; set; }

        /// <summary>
        /// Dit is een groepsles waarop leden kunnen aanmelden en/of afmelden
        /// </summary>
        /// <param name="name">Naam van de groepsles</param>
        /// <param name="iD">Database ID groepsles</param>
        /// <param name="duration">Aantal minuten dat de les duurt</param>
        /// <param name="date">De datum waarop de les is</param>
        /// <param name="instructor">De trainer die de groep begeleid</param>
        /// <param name="workout">Het type workout waar de les van is</param>
        public GroupClass(string name, long iD, int duration, DateTime date, Instructor instructor, Workout workout)
        {
            Name = name;
            ID = iD;
            Duration = duration;
            Date = date;
            Instructor = instructor;
            Workout = workout;
        }

        /// <summary>
        /// In deze constructor wordt DTO omgezet naar domeinklasse
        /// </summary>
        /// <param name="dto">De groupclass DTO die wordt omgezet</param>
        public GroupClass(GroupClassDTO dto)
        {
            Name = dto.Name;
            ID = dto.ID;
            Duration = dto.Duration;
            Date = dto.Date;
            Instructor = new Instructor(dto.Instructor);
            Workout = new Workout(dto.Workout);
        }

        /// <summary>
        /// Hier wordt de domeinklasse omgezet naar DTO
        /// </summary>
        /// <returns>Een groupclass DTO</returns>
        public GroupClassDTO GetDTO()
        {
            GroupClassDTO dto = new GroupClassDTO(Name, ID, Duration, Date, Instructor.GetDTO(), Workout.GetDTO());
            return dto;
        }
    }
}
