﻿using InterfaceLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusnLogicGL
{
    public class WorkoutContainer
    {
        private readonly IWorkoutContainer DALLaagReferentie;

        public WorkoutContainer(IWorkoutContainer dALLaagReferentie)
        {
            this.DALLaagReferentie = dALLaagReferentie;
        }

        public void Create(Workout g)
        {
            WorkoutDTO dto = g.GetDTO();
            DALLaagReferentie.Create(dto);
        }

        public Workout FindByID(long ID)
        {
            WorkoutDTO dto = DALLaagReferentie.FindByID(ID);
            if(dto != null)
            {
                return new Workout(dto);
            }
            return null;
        }

        public void Update(Workout g)
        {
            WorkoutDTO dto = g.GetDTO();
            DALLaagReferentie.Update(dto);
        }

        public void Delete(Workout g)
        {
            WorkoutDTO dto = g.GetDTO();
            DALLaagReferentie.Delete(dto);
        }

        public List<Workout> GetAll()
        {
            List<WorkoutDTO> dtos = DALLaagReferentie.GetAll();
            List<Workout> workouts = new List<Workout>();
            foreach (WorkoutDTO dto in dtos)
            {
                workouts.Add(new Workout(dto));
            }
            return workouts;
        }
    }
}
