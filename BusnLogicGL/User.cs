﻿using InterfaceLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusnLogicGL
{
    public class User
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string PhoneNumber { get; set; }
        public long ID { get; }
        public bool IsAdmin { get; }

        /// <summary>
        /// Dit is een gebruiker van de applicatie. Dit kan zowel een administrator als een lid zijn.
        /// </summary>
        /// <param name="firstName">Voornaam van de gebruiker</param>
        /// <param name="lastName">Achternaam van de gebruiker</param>
        /// <param name="addressLine1">Regel 1 van het adres van de gebruiker</param>
        /// <param name="addressLine2">Regel 2 van het adres van de gebruiker</param>
        /// <param name="dateOfBirth">De geboortedatum van de gebruiker</param>
        /// <param name="email">Email van de gebruiker waarmee wordt ingelogd</param>
        /// <param name="password">Wachtwoord van de gebruiker waarmee wordt ingelogd</param>
        /// <param name="phoneNumber">Telefoonnummer van de gebruiker</param>
        /// <param name="iD">Database ID van de gebruiker</param>
        /// <param name="isAdmin">Geeft aan of de gebruiker een admin of normaal lid is</param>
        public User(string firstName, string lastName, string addressLine1, string addressLine2, 
        DateTime dateOfBirth, string email, string password, string phoneNumber, long iD, bool isAdmin)
        {
            FirstName = firstName;
            LastName = lastName;
            AddressLine1 = addressLine1;
            AddressLine2 = addressLine2;
            DateOfBirth = dateOfBirth;
            Email = email;
            Password = password;
            PhoneNumber = phoneNumber;
            ID = iD;
            IsAdmin = isAdmin;
        }

        /// <summary>
        /// In deze constructor wordt DTO omgezet naar domeinklasse
        /// </summary>
        /// <param name="dto">De user DTO die wordt omgezet</param>
        public User(UserDTO dto)
        {
            FirstName = dto.FirstName;
            LastName = dto.LastName;
            AddressLine1 = dto.AddressLine1;
            AddressLine2 = dto.AddressLine2;
            DateOfBirth = dto.DateOfBirth;
            Email = dto.Email;
            Password = dto.Password;
            PhoneNumber = dto.PhoneNumber;
            ID = dto.ID;
            IsAdmin = dto.IsAdmin;
        }

        /// <summary>
        /// Hier wordt de domeinklasse omgezet naar DTO
        /// </summary>
        /// <returns>Een user DTO</returns>
        public UserDTO GetDTO()
        {
            UserDTO dto = new UserDTO(FirstName, LastName, AddressLine1, AddressLine2, DateOfBirth, Email, Password, PhoneNumber, ID, IsAdmin);
            return dto;
        }

        public override string? ToString()
        {
            return $"{ID}" + $" {FirstName}" + $" {LastName}";
        }
    }
}
