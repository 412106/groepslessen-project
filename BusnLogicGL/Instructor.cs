﻿using InterfaceLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusnLogicGL
{
    public class Instructor
    {
        public string Name { get; set; }
        public long ID { get; }
        public string Specialty { get; set; }

        /// <summary>
        /// Dit is een instructeur die een groepsles kan geven
        /// </summary>
        /// <param name="name">Naam van de instructeur</param>
        /// <param name="iD">Database ID van de instructeur</param>
        /// <param name="specialty">De instructeur's specialiteit</param>
        public Instructor(string name, long iD, string specialty)
        {
            Name = name;
            ID = iD;
            Specialty = specialty;
        }

        /// <summary>
        /// In deze constructor wordt DTO omgezet naar domeinklasse
        /// </summary>
        /// <param name="dto">De instructor DTO die wordt omgezet</param>
        public Instructor(InstructorDTO dto)
        {
            Name = dto.Name;
            ID = dto.ID;
            Specialty = dto.Specialty;
        }

        /// <summary>
        /// Hier wordt de domeinklasse omgezet naar DTO
        /// </summary>
        /// <returns>Een instructor DTO</returns>
        public InstructorDTO GetDTO()
        {
            InstructorDTO dto = new InstructorDTO(Name, ID, Specialty);
            return dto;
        }
    }
}
