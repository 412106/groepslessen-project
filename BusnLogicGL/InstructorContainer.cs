﻿using InterfaceLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusnLogicGL
{
    public class InstructorContainer
    {
        private readonly IInstructorContainer DALLaagReferentie;

        public InstructorContainer(IInstructorContainer dALLaagReferentie)
        {
            this.DALLaagReferentie = dALLaagReferentie;
        }

        public void Create(Instructor i)
        {
            InstructorDTO dto = i.GetDTO();
            DALLaagReferentie.Create(dto);
        }

        public Instructor FindByID(long ID)
        {
            InstructorDTO dto = DALLaagReferentie.FindByID(ID);
            return new Instructor(dto);
        }

        public void Update(Instructor i)
        {
            InstructorDTO dto = i.GetDTO();
            DALLaagReferentie.Update(dto);
        }

        public void Delete(Instructor i)
        {
            InstructorDTO dto = i.GetDTO();
            DALLaagReferentie.Delete(dto);
        }

        public List<Instructor> GetAll()
        {
            List<InstructorDTO> dtos = DALLaagReferentie.GetAll();
            List<Instructor> instructors = new List<Instructor>();
            foreach (InstructorDTO dto in dtos)
            {
                instructors.Add(new Instructor(dto));
            }
            return instructors;
        }
    }
}

