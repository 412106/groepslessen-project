﻿function ChooseInstructor() {
    var text = document.getElementById('instructors');
    var opties = text.options[text.selectedIndex];
    document.getElementById('textI').value = opties.text;
    document.getElementById('insID').value = opties.value;
}

function ChooseWorkout() {
    var text = document.getElementById('workouts');
    var opties = text.options[text.selectedIndex];
    document.getElementById('textW').value = opties.text;
    document.getElementById('workoutID').value = opties.value;
}

function ToggleVisibility() {
    var x = document.getElementById("Password");
    if (x.type === "password") {
        x.type = "text";
    } else {
        x.type = "password";
    }
}

$(document).ready(function () {
    $("#myInput").on("keyup", function () {
        var value = $(this).val().toLowerCase();
        $("#myTable tr").filter(function () {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
    });
});

$(function () {
    var PlaceHolderElement = $('#PlaceHolderHere');
    $('button[data-toggle="ajax-modal"]').click(function myfunction() {
        var url = $(this).data('url');
        var decorderdUrl = decodeURIComponent(url);
        $.get(decorderdUrl).done(function (data) {
            PlaceHolderElement.html(data);
            PlaceHolderElement.find('.modal').modal('show');
        })
    })

    PlaceHolderElement.on('click', '[data-save="modal"]', function (event) {
        var form = $(this).parents('.modal').find('form');
        var actionUrl = form.attr('action');
        var sendData = form.serialize();
        $.post(actionUrl, sendData).done(function (data) {
            PlaceHolderElement.find('.modal').modal('hide');
            location.reload();
        })
    })
});

$(function () {
    $('#myForm').submit(function () {
        var myField = $('#tel').val();
        if (/^\d{10}$/.test(myField) === false) {
            alert('10 digit code is required');
            return false;
        }
        $('#myForm').submit();
        return false;
    });
});