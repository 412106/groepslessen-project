﻿using BusnLogicGL;
using UTDAL;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Semester2GroepslessenASP.Models
{
    public class GroupClassVM
    {
        public string Name { get; set; }
        public long ID { get; set; }
        public int Duration { get; set; }
        public DateTime Date { get; set; }
        public InstructorVM Instructor { get; set; }
        public WorkoutVM Workout { get; set; }
        public bool? IsSignedUp { get; set; }

        public GroupClassVM(string name, long iD, int duration, DateTime date, InstructorVM instructor, WorkoutVM workout, bool isSignedUp)
        {
            Name = name;
            ID = iD;
            Duration = duration;
            Date = date;
            if (instructor != null)
            {
                Instructor = instructor;
            }
            if (workout != null)
            {
                Workout = workout;
            }
            IsSignedUp = isSignedUp;
        }

        public GroupClassVM(GroupClass g, bool isSignedUp)
        {
            Name = g.Name;
            ID = g.ID;
            Duration = g.Duration;
            Date = g.Date;
            if (g.Instructor != null)
            {
                this.Instructor = new(g.Instructor.Name, g.Instructor.ID, g.Instructor.Specialty);
            }
            if (g.Workout != null)
            {
                this.Workout = new(g.Workout.Name, g.Workout.Description, g.Workout.ID, g.Workout.URL);
            }
            IsSignedUp = isSignedUp;
        }

        public GroupClassVM(GroupClass g)
        {
            Name = g.Name;
            ID = g.ID;
            Duration = g.Duration;
            Date = g.Date;
            if (g.Instructor != null)
            {
                this.Instructor = new(g.Instructor.Name, g.Instructor.ID, g.Instructor.Specialty);
            }
            if (g.Workout != null)
            {
                this.Workout = new(g.Workout.Name, g.Workout.Description, g.Workout.ID, g.Workout.URL);
            }
        }

        public GroupClassVM()
        {

        }
    }
}
