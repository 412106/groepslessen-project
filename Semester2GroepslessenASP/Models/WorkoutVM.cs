﻿using BusnLogicGL;

namespace Semester2GroepslessenASP.Models
{
    public class WorkoutVM
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public long ID { get; set; }
        public string URL { get; set; }

        public WorkoutVM(string name, string description, long iD, string uRL)
        {
            Name = name;
            Description = description;
            ID = iD;
            URL = uRL;
        }

        public WorkoutVM(Workout workout)
        {
            Name = workout.Name;
            Description = workout.Description;
            ID = workout.ID;
            URL = workout.URL;
        }

        public WorkoutVM()
        {
        }

        public override string ToString()
        {
            return $"{Name}";
        }
    }
}
