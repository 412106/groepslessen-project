﻿using BusnLogicGL;
using System.ComponentModel.DataAnnotations;

namespace Semester2GroepslessenASP.Models
{
    public class UserVM
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        [Required(ErrorMessage = "You must provide a phone number")]
        [Display(Name = "Phone")]
        [DataType(DataType.PhoneNumber)]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Not a valid phone number")]
        public string PhoneNumber { get; set; }
        public long ID { get; set; }
        public bool IsAdmin { get; }

        public UserVM(string firstName, string lastName, string addressLine1, string addressLine2,
            DateTime dateOfBirth, string email, string password, string phoneNumber, long iD, bool isAdmin)
        {
            FirstName = firstName;
            LastName = lastName;
            AddressLine1 = addressLine1;
            AddressLine2 = addressLine2;
            DateOfBirth = dateOfBirth;
            Email = email;
            Password = password;
            PhoneNumber = phoneNumber;
            ID = iD;
            IsAdmin = isAdmin;
        }

        public UserVM(User user)
        {
            FirstName = user.FirstName;
            LastName = user.LastName;
            AddressLine1 = user.AddressLine1;
            AddressLine2 = user.AddressLine2;
            DateOfBirth = user.DateOfBirth;
            Email = user.Email;
            Password = user.Password;
            PhoneNumber = user.PhoneNumber;
            ID = user.ID;
            IsAdmin = user.IsAdmin;
        }
        public override string? ToString()
        {
            return $"{Email}" + $" {Password}";
        }

        public UserVM()
        {

        }
    }
}
