﻿using BusnLogicGL;
using UTDAL;

namespace Semester2GroepslessenASP.Models
{
    public class GroupClassInstructorsWorkoutsVM
    {
        public List<InstructorVM> Instructors { get; set; } = new List<InstructorVM>();
        public GroupClassVM GroupClass { get; set; }
        public InstructorVM Instructor { get; set; }
        public List<WorkoutVM> Workouts { get; set; } = new List<WorkoutVM>();
        public WorkoutVM Workout { get; set; }

        public GroupClassInstructorsWorkoutsVM()
        {

        }
    }
}
