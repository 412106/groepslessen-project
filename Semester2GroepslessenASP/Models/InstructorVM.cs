﻿using BusnLogicGL;

namespace Semester2GroepslessenASP.Models
{
    public class InstructorVM
    {
        public string Name { get; set; }
        public long ID { get; set; }
        public string Specialty { get; set; }

        public InstructorVM(string name, long iD, string specialty)
        {
            Name = name;
            ID = iD;
            Specialty = specialty;
        }
        public InstructorVM()
        {

        }
        public InstructorVM(Instructor instructor)
        {
            Name = instructor.Name;
            ID = instructor.ID;
            Specialty = instructor.Specialty;
        }

        public override string ToString()
        {
            return $"{Name}";
        }
    }
}
