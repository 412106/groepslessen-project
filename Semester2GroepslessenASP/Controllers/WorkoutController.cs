﻿using BusnLogicGL;
using UTDAL;
using Microsoft.AspNetCore.Mvc;
using Semester2GroepslessenASP.Models;

namespace Semester2GroepslessenASP.Controllers
{
    public class WorkoutController : Controller
    {
        WorkoutContainer wc = new WorkoutContainer(new WorkoutMSSQLDAL());

        /// <summary>
        /// Haalt alle workouts op, maakt hier WorkoutVMs van, voegt deze toe aan een lijst
        /// en stuurt dit door naar de view.
        /// </summary>
        /// <returns>De index view, waaran de workouts worden meegegeven</returns>
        public IActionResult Index()
        {
            try
            {
                if (HttpContext.Session.GetString("IsAdmin") == "True")
                {
                    List<Workout> workouts = wc.GetAll();
                    List<WorkoutVM> vms = new List<WorkoutVM>();
                    foreach (Workout w in workouts)
                    {
                        vms.Add(new WorkoutVM(w));
                    }
                    return View(vms);
                }
                else
                {
                    return RedirectToAction("Login", "Home");
                }
            }
            catch (TemporaryException ex)
            {
                ViewBag.Message = ex.Message;
                return View("SqlError");
            }
            catch (PermanentException ex)
            {
                ViewBag.Message = ex.Message;
                return View("PermanentError");
            }
        }

        /// <summary>
        /// Deze methode wordt aangeroepen wanneer de user een nieuwe workout aan wil maken
        /// </summary>
        /// <returns>De CreateWorkout view</returns>
        public IActionResult CreateWorkout()
        {
            try
            {
                if (HttpContext.Session.GetString("IsAdmin") == "True")
                {
                    return View();
                }
                else
                {
                    return RedirectToAction("Login", "Home");
                }
            }
            catch (TemporaryException ex)
            {
                ViewBag.Message = ex.Message;
                return View("SqlError");
            }
            catch (PermanentException ex)
            {
                ViewBag.Message = ex.Message;
                return View("PermanentError");
            }
        }

        /// <summary>
        /// Maakt een nieuwe workout aan
        /// </summary>
        /// <param name="vm">De details van de workout</param>
        /// <returns>De geüpdate index pagina met de nieuwe workout toegevoegd</returns>
        [HttpPost]
        public IActionResult CreateWorkout(WorkoutVM vm)
        {
            try
            {
                Workout w = new Workout(vm.Name, vm.Description, vm.ID, vm.URL);
                wc.Create(w);
                return RedirectToAction("Index");
            }
            catch (TemporaryException ex)
            {
                ViewBag.Message = ex.Message;
                return View("SqlError");
            }
            catch (PermanentException ex)
            {
                ViewBag.Message = ex.Message;
                return View("PermanentError");
            }
        }

        /// <summary>
        /// Haalt de gegevens op van de workout die bij het ID hoort, maakt van de workout een
        /// WorkoutVM en geeft deze mee aan de view
        /// </summary>
        /// <param name="id">Het ID van de workout</param>
        /// <returns>De UpdateWorkout view met de WorkoutVM</returns>
        public IActionResult UpdateWorkout(int id)
        {
            try
            {
                if (HttpContext.Session.GetString("IsAdmin") == "True")
                {
                    Workout w = wc.FindByID(id);
                    WorkoutVM vm = new WorkoutVM(w);
                    return View(vm);
                }
                else
                {
                    return RedirectToAction("Login", "Home");
                }
            }
            catch (TemporaryException ex)
            {
                ViewBag.Message = ex.Message;
                return View("SqlError");
            }
            catch (PermanentException ex)
            {
                ViewBag.Message = ex.Message;
                return View("PermanentError");
            }
        }

        /// <summary>
        /// Update de workout
        /// </summary>
        /// <param name="vm">De meegegeven workout</param>
        /// <returns>de Index view</returns>
        [HttpPost]
        public IActionResult UpdateWorkout(WorkoutVM vm)
        {
            try
            {
                Workout w = new Workout(vm.Name, vm.Description, vm.ID, vm.URL);
                wc.Update(w);
                return RedirectToAction("Index");
            }
            catch (TemporaryException ex)
            {
                ViewBag.Message = ex.Message;
                return View("SqlError");
            }
            catch (PermanentException ex)
            {
                ViewBag.Message = ex.Message;
                return View("PermanentError");
            }
        }

        /// <summary>
        /// Haalt de gegevens van de workout op die bij het meegegeven ID hoort, maakt hier een
        /// WorkoutVM van en geeft deze mee aan de view
        /// </summary>
        /// <param name="id">Het ID van de workout</param>
        /// <returns>De DeleteWorkout view met de WorkoutVM</returns>
        [HttpGet]
        public IActionResult DeleteWorkout(int id)
        {
            try
            {
                if (HttpContext.Session.GetString("IsAdmin") == "True")
                {
                    Workout w = wc.FindByID(id);
                    WorkoutVM vm = new WorkoutVM(w);
                    return PartialView(vm);
                }
                else
                {
                    return RedirectToAction("Login", "Home");
                }
            }
            catch (TemporaryException ex)
            {
                ViewBag.Message = ex.Message;
                return View("SqlError");
            }
            catch (PermanentException ex)
            {
                ViewBag.Message = ex.Message;
                return View("PermanentError");
            }
        }

        /// <summary>
        /// Delete de workout
        /// </summary>
        /// <param name="vm">De meegegeven workout</param>
        /// <returns>De geüpdate index view met de workout verwijderd</returns>
        [HttpPost]
        public IActionResult DeleteWorkout(WorkoutVM vm)
        {
            try
            {
                Workout w = new(vm.Name, vm.Description, vm.ID, vm.URL);
                wc.Delete(w);
                return RedirectToAction("Index");
            }
            catch (TemporaryException ex)
            {
                ViewBag.Message = ex.Message;
                return View("SqlError");
            }
            catch (PermanentException ex)
            {
                ViewBag.Message = ex.Message;
                return View("PermanentError");
            }
        }
    }
}
