﻿using BusnLogicGL;
using UTDAL;
using Microsoft.AspNetCore.Mvc;
using Semester2GroepslessenASP.Models;

namespace Semester2GroepslessenASP.Controllers
{
    public class GroupClassController : Controller
    {
        GroupClassContainer gc = new GroupClassContainer(new GroupClassMSSQLDAL());
        UserContainer uc = new UserContainer(new UserMSSQLDAL());
        InstructorContainer ic = new InstructorContainer(new InstructorMSSQLDAL());
        WorkoutContainer wc = new WorkoutContainer(new WorkoutMSSQLDAL());
        AttendanceContainer ac = new AttendanceContainer(new AttendanceMSSQLDAL());

        /// <summary>
        /// Haalt alle groupclasses op, maakt hier GroupClassVMs van, voegt deze toe aan een lijst
        /// en stuurt dit door naar de view.
        /// </summary>
        /// <returns>De index view, waaran de groupclasses worden meegegeven</returns>
        [HttpGet]
        public IActionResult Index()
        {
            try
            {
                int userID = Convert.ToInt32(HttpContext.Session.GetString("ID"));
                List<GroupClassVM> vms = new();
                List<GroupClass> classes = gc.GetAll();
                foreach (GroupClass g in classes)
                {
                    bool IsSignedUp = ac.IsAttending(userID, g.ID);
                    vms.Add(new GroupClassVM(g, IsSignedUp));
                }
                return View(vms);
            }
            catch (TemporaryException ex)
            {
                ViewBag.Message = ex.Message;
                return View("SqlError");
            }
            catch (PermanentException ex)
            {
                ViewBag.Message = ex.Message;
                return View("PermanentError");
            }
        }

        /// <summary>
        /// Haalt de gegevens op voor de groupclass waarvan het ID is meegegeven aan de view en
        /// maakt hier een GroupClassVM van
        /// </summary>
        /// <param name="iD">Het ID van de groupclass</param>
        /// <returns>De SignUp view en de groupclass</returns>
        [HttpGet]
        public IActionResult SignUp(int iD)
        {
            try
            {
                GroupClass g = gc.FindByID(iD);
                GroupClassVM vm = new GroupClassVM(g);
                return PartialView(vm);
            }
            catch (TemporaryException ex)
            {
                ViewBag.Message = ex.Message;
                return View("SqlError");
            }
            catch (PermanentException ex)
            {
                ViewBag.Message = ex.Message;
                return View("PermanentError");
            }
        }

        /// <summary>
        /// Deze methode wordt aangeroepen wanneer een lid zich aanmeld voor een groepsles.
        /// Je moet ingelogd zijn om deze methode te kunnen aanroepen
        /// </summary>
        /// <param name="gm">Groepsles waarbij die zich probeert aan te melden</param>
        /// <returns>De tekst "SignedUp" bij succes, anders melding dat er ingelogd moet worden</returns>
        [HttpPost]
        public IActionResult SignUp(GroupClassVM gm)
        {
            try
            {
                long userID = Convert.ToInt64(HttpContext.Session.GetString("ID"));
                if (userID != 0)
                {
                    ac.SignUp(userID, Convert.ToInt32(gm.ID));
                    return RedirectToAction("Index");
                }
                else
                {
                    return Content("You're not logged in");
                }
            }
            catch (TemporaryException ex)
            {
                ViewBag.Message = ex.Message;
                return View("SqlError");
            }
            catch (PermanentException ex)
            {
                ViewBag.Message = ex.Message;
                return View("PermanentError");
            }
        }

        /// <summary>
        /// Alle instructors en workouts worden hier meegegeven aan de view
        /// </summary>
        /// <returns>De CreateGC view met de InstructorVMs en WorkoutVMs</returns>
        public IActionResult CreateGC()
        {
            try
            {
                if (HttpContext.Session.GetString("IsAdmin") == "True")
                {
                    GroupClassInstructorsWorkoutsVM vms = new();
                    List<Instructor> instructors = ic.GetAll();
                    List<Workout> workouts = wc.GetAll();
                    foreach (var instructor in instructors)
                    {
                        vms.Instructors.Add(new InstructorVM(instructor));
                    }
                    foreach (var workout in workouts)
                    {
                        vms.Workouts.Add(new WorkoutVM(workout));
                    }
                    return View(vms);
                }
                else
                {
                    return RedirectToAction("Login", "Home");
                }
            }
            catch (TemporaryException ex)
            {
                ViewBag.Message = ex.Message;
                return View("SqlError");
            }
            catch (PermanentException ex)
            {
                ViewBag.Message = ex.Message;
                return View("PermanentError");
            }
        }

        /// <summary>
        /// Maakt een nieuwe groupclass aan
        /// </summary>
        /// <param name="gm">De details van de groupclass</param>
        /// <returns>De geüpdate index pagina met de nieuwe groupclass toegevoegd</returns>
        [HttpPost]
        public IActionResult CreateGC(GroupClassInstructorsWorkoutsVM gm)
        {
            try
            {
                GroupClass g = new GroupClass(gm.GroupClass.Name, gm.GroupClass.ID, gm.GroupClass.Duration, gm.GroupClass.Date,
                new Instructor(gm.GroupClass.Instructor.Name, gm.GroupClass.Instructor.ID, gm.GroupClass.Instructor.Specialty), new Workout(gm.GroupClass.Workout.Name, gm.GroupClass.Workout.Description, gm.GroupClass.Workout.ID, gm.GroupClass.Workout.URL));
                gc.Create(g);
                return RedirectToAction("Index");
            }
            catch (TemporaryException ex)
            {
                ViewBag.Message = ex.Message;
                return View("SqlError");
            }
            catch (PermanentException ex)
            {
                ViewBag.Message = ex.Message;
                return View("PermanentError");
            }
        }

        /// <summary>
        /// Alle instructors en workouts worden hier opgehaald en omgezet. De info van de groupclass
        /// die bij het ID hoort wordt opgehaald, van de groupclass wordt een GroupclassVM gemaakt en
        /// een lijst met instructors, workouts en de groupclass wordt meegegeven aan de view.
        /// </summary>
        /// <param name="iD">Het ID van de groupclass</param>
        /// <returns>De UpdateGC view met de instructorVMs, WorkoutVMs en GroupClassVM</returns>
        [HttpGet]
        public IActionResult UpdateGC(int iD)
        {
            try
            {
                if (HttpContext.Session.GetString("IsAdmin") == "True")
                {
                    GroupClassInstructorsWorkoutsVM vms = new();
                    List<Instructor> instructors = ic.GetAll();
                    List<Workout> workouts = wc.GetAll();
                    foreach (var instructor in instructors)
                    {
                        vms.Instructors.Add(new InstructorVM(instructor));
                    }
                    foreach (var workout in workouts)
                    {
                        vms.Workouts.Add(new WorkoutVM(workout));
                    }
                    GroupClass g = gc.FindByID(iD);
                    vms.GroupClass = new GroupClassVM(g);
                    return View(vms);
                }
                else
                {
                    return RedirectToAction("Login", "Home");
                }
            }
            catch (TemporaryException ex)
            {
                ViewBag.Message = ex.Message;
                return View("SqlError");
            }
            catch (PermanentException ex)
            {
                ViewBag.Message = ex.Message;
                return View("PermanentError");
            }
        }

        /// <summary>
        /// Updatet de groupclass
        /// </summary>
        /// <param name="gm">De meegegeven GroupclassVM</param>
        /// <returns>De Index view</returns>
        [HttpPost]
        public IActionResult UpdateGC(GroupClassInstructorsWorkoutsVM gm)
        {
            try
            {
                GroupClass g = new GroupClass(gm.GroupClass.Name, gm.GroupClass.ID, gm.GroupClass.Duration, gm.GroupClass.Date, new Instructor(gm.GroupClass.Instructor.Name, gm.GroupClass.Instructor.ID, gm.GroupClass.Instructor.Specialty), new Workout(gm.GroupClass.Workout.Name, gm.GroupClass.Workout.Description, gm.GroupClass.Workout.ID, gm.GroupClass.Workout.URL));
                gc.Update(g);
                return RedirectToAction("Index");
            }
            catch (TemporaryException ex)
            {
                ViewBag.Message = ex.Message;
                return View("SqlError");
            }
            catch (PermanentException ex)
            {
                ViewBag.Message = ex.Message;
                return View("PermanentError");
            }
        }

        /// <summary>
        /// Haalt alle groupclasses op, maakt hier GroupClassVMs van, voegt deze toe aan een lijst
        /// en geeft deze mee aan de view.
        /// </summary>
        /// <returns>De ClassAttendance view met de GroupClassVMs</returns>
        [HttpGet]
        public IActionResult ClassAttendance()
        {
            try
            {
                if (HttpContext.Session.GetString("IsAdmin") == "True")
                {
                    List<GroupClass> classes = gc.GetAll();
                    List<GroupClassVM> vms = new List<GroupClassVM>();
                    foreach (GroupClass g in classes)
                    {
                        vms.Add(new GroupClassVM(g));
                    }
                    return View(vms);
                }
                else
                {
                    return RedirectToAction("Login", "Home");
                }
            }
            catch (TemporaryException ex)
            {
                ViewBag.Message = ex.Message;
                return View("SqlError");
            }
            catch (PermanentException ex)
            {
                ViewBag.Message = ex.Message;
                return View("PermanentError");
            }
        }

        /// <summary>
        /// Haalt alle users op die zich hebben aangemeld voor de meegegeven groupclass, maakt hier
        /// UserVMs van, voegt deze toe aan een lijst en geeft deze mee aan de view.
        /// </summary>
        /// <param name="iD">Het ID van de groupclass</param>
        /// <returns>De CheckAttendance view met de UserVMs</returns>
        [HttpGet]
        public IActionResult CheckAttendance(int iD)
        {
            try
            {
                if (HttpContext.Session.GetString("IsAdmin") == "True")
                {
                    List<User> users = ac.FindAllSignedUpUsers(iD);
                    List<UserVM> vms = new();
                    foreach (User u in users)
                    {
                        vms.Add(new UserVM(u));
                    }
                    return View(vms);
                }
                else
                {
                    return RedirectToAction("Login", "Home");
                }
            }
            catch (TemporaryException ex)
            {
                ViewBag.Message = ex.Message;
                return View("SqlError");
            }
            catch (PermanentException ex)
            {
                ViewBag.Message = ex.Message;
                return View("PermanentError");
            }
        }

        /// <summary>
        /// Haalt de gegevens op voor de groupclass die is meegegeven, maakt hier een GroupClassVM van
        /// en geeft deze mee aan de view.
        /// </summary>
        /// <param name="iD">Het ID van de groupclass</param>
        /// <returns>De Cancel view met de GroupClassVM</returns>
        [HttpGet]
        public IActionResult Cancel(int iD)
        {
            try
            {
                GroupClass g = gc.FindByID(iD);
                GroupClassVM vm = new GroupClassVM(g);
                return PartialView(vm);
            }
            catch (TemporaryException ex)
            {
                ViewBag.Message = ex.Message;
                return View("SqlError");
            }
            catch (PermanentException ex)
            {
                ViewBag.Message = ex.Message;
                return View("PermanentError");
            }
        }

        /// <summary>
        /// Checkt het ID van de user, wanneer deze niet null is en dus is ingelogd, annuleert de
        /// aanmelding van de gebruiker voor de meegegeven groupclass.
        /// </summary>
        /// <param name="gm">De groupclass waar de user zich voor wil afmelden</param>
        /// <returns>De view "cancelled" wanneer de user is ingelogd, "You're not logged in"
        /// wanneer de user niet is ingelogd</returns>
        [HttpPost]
        public IActionResult Cancel(GroupClassVM gm)
        {
            try
            {
                long userID = Convert.ToInt64(HttpContext.Session.GetString("ID"));
                if (userID != 0)
                {
                    ac.Cancel(userID, Convert.ToInt64(gm.ID));
                    return RedirectToAction("Index");
                }
                else
                {
                    return Content("You're not logged in");
                }
            }
            catch (TemporaryException ex)
            {
                ViewBag.Message = ex.Message;
                return View("SqlError");
            }
            catch (PermanentException ex)
            {
                ViewBag.Message = ex.Message;
                return View("PermanentError");
            }
        }

        /// <summary>
        /// Deze view wordt aangeroepen bij een SqlException
        /// </summary>
        /// <returns>De ConnectionErrorMessage view</returns>
        [HttpGet]
        public IActionResult SqlErrror()
        {
            return View();
        }

        /// <summary>
        /// Haalt de informatie op voor de groupclass waarvan het ID is meegegeven, maakt hier een
        /// GroupClassVM van en geeft deze mee aan de view.
        /// </summary>
        /// <param name="id">Het ID van de groupclass</param>
        /// <returns>De DeleteGC view met de groupclassVM</returns>
        [HttpGet]
        public IActionResult DeleteGC(int id)
        {
            try
            {
                if (HttpContext.Session.GetString("IsAdmin") == "True")
                {
                    GroupClass g = gc.FindByID(id);
                    GroupClassVM vm = new GroupClassVM(g);
                    return PartialView(vm);
                }
                else
                {
                    return RedirectToAction("Login", "Home");
                }
            }
            catch (TemporaryException ex)
            {
                ViewBag.Message = ex.Message;
                return View("SqlError");
            }
            catch (PermanentException ex)
            {
                ViewBag.Message = ex.Message;
                return View("PermanentError");
            }
        }

        /// <summary>
        /// Delete de groupclass
        /// </summary>
        /// <param name="vm">De meegegeven groupclass</param>
        /// <returns>De geüpdate index view met de groupclass verwijderd</returns>
        [HttpPost]
        public IActionResult DeleteGC(GroupClassVM vm)
        {
            try
            {
                GroupClass g = new(vm.Name, vm.ID, vm.Duration, vm.Date, new Instructor(vm.Instructor.Name, vm.Instructor.ID, vm.Instructor.Specialty), new Workout(vm.Workout.Name, vm.Workout.Description, vm.Workout.ID, vm.Workout.URL));
                gc.Delete(g);
                return RedirectToAction("Index");
            }
            catch (TemporaryException ex)
            {
                ViewBag.Message = ex.Message;
                return View("SqlError");
            }
            catch (PermanentException ex)
            {
                ViewBag.Message = ex.Message;
                return View("PermanentError");
            }
        }

        public IActionResult WorkoutInfo(int id)
        {
            GroupClass g = gc.FindByID(id);
            GroupClassVM vm = new GroupClassVM(g);
            return PartialView(vm);
        }
    }
}
