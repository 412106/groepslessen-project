﻿using Microsoft.AspNetCore.Mvc;
using Semester2GroepslessenASP.Models;
using System.Diagnostics;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using BusnLogicGL;
using UTDAL;

namespace Semester2GroepslessenASP.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        UserContainer uc = new UserContainer(new UserMSSQLDAL());


        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        /// <summary>
        /// Deze view wordt aangeroepen bij het starten van de applicatie
        /// </summary>
        /// <returns>De Index view</returns>
        public IActionResult Index()
        {
            try
            {
                return View();
            }
            catch (TemporaryException ex)
            {
                ViewBag.Message = ex.Message;
                return View("SqlError");
            }
            catch (PermanentException ex)
            {
                ViewBag.Message = ex.Message;
                return View("PermanentError");
            }
        }

        public IActionResult Privacy()
        {
            return View();
        }

        /// <summary>
        /// Deze view wordt weergeven wanneer de user op Log In klikt
        /// </summary>
        /// <returns>De Login view</returns>
        [HttpGet]
        public IActionResult Login()
        {
            try
            {
                return View();
            }
            catch (TemporaryException ex)
            {
                ViewBag.Message = ex.Message;
                return View("SqlError");
            }
            catch (PermanentException ex)
            {
                ViewBag.Message = ex.Message;
                return View("PermanentError");
            }
        }

        /// <summary>
        /// De email en het wachtwoord worden naar de database gestuurd en er wordt gekeken of er een
        /// match is. Als deze er niet is, wordt er een view returned die aangeeft dat de gegevens
        /// onjuist zijn. Als deze juist zijn, wordt er gekeken of deze user een admin is of een
        /// normale user.
        /// </summary>
        /// <param name="vm">De inlog viewmodel</param>
        /// <returns>LoginError view als er geen match is, de index view van User als deze niet null is</returns>

        [HttpPost]
        public IActionResult Login(InlogVM vm)
        {
            try
            {
                User user = uc.FindByEmailAndPassword(vm.Email, vm.Password);
                if (user == null)
                {
                    return View("LoginError");
                }
                else
                {
                    if (user.IsAdmin)
                    {
                        HttpContext.Session.SetString("IsAdmin", user.IsAdmin.ToString());
                    }
                    HttpContext.Session.SetString("ID", user.ID.ToString());
                    return RedirectToAction("Index", "User");
                }
            }
            catch (TemporaryException ex)
            {
                ViewBag.Message = ex.Message;
                return View("SqlError");
            }
            catch (PermanentException ex)
            {
                ViewBag.Message = ex.Message;
                return View("PermanentError");
            }
        }

        /// <summary>
        /// Beëindigd de session
        /// </summary>
        /// <returns>De index view</returns>
        public IActionResult Logout()
        {
            HttpContext.Session.Clear();
            return View("Index");
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}