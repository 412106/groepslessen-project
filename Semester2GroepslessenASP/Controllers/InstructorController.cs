﻿using BusnLogicGL;
using UTDAL;
using Microsoft.AspNetCore.Mvc;
using Semester2GroepslessenASP.Models;

namespace Semester2GroepslessenASP.Controllers
{
    public class InstructorController : Controller
    {
        InstructorContainer ic = new InstructorContainer(new InstructorMSSQLDAL());

        /// <summary>
        /// Haalt alle instructors op, maakt hier InstructorVMs van, voegt deze toe aan een lijst
        /// en stuurt dit door naar de view.
        /// </summary>
        /// <returns>De index view, waaran de instructors worden meegegeven</returns>
        public IActionResult Index()
        {
            try
            {
                if (HttpContext.Session.GetString("IsAdmin") == "True")
                {
                    List<Instructor> instructors = ic.GetAll();
                    List<InstructorVM> vms = new List<InstructorVM>();
                    foreach (Instructor i in instructors)
                    {
                        vms.Add(new InstructorVM(i.Name, i.ID, i.Specialty));
                    }
                    return View(vms);
                }
                else
                {
                    return RedirectToAction("Login", "Home");
                }
            }
            catch (TemporaryException ex)
            {
                ViewBag.Message = ex.Message;
                return View("SqlError");
            }
            catch (PermanentException ex)
            {
                ViewBag.Message = ex.Message;
                return View("PermanentError");
            }
        }

        /// <summary>
        /// Deze methode wordt aangeroepen wanneer de user een nieuwe instructor wil aanmaken
        /// </summary>
        /// <returns>De CreateInstructor view</returns>
        public IActionResult CreateInstructor()
        {
            try
            {
                if (HttpContext.Session.GetString("IsAdmin") == "True")
                {
                    return View();
                }
                else
                {
                    return RedirectToAction("Login", "Home");
                }
            }
            catch (TemporaryException ex)
            {
                ViewBag.Message = ex.Message;
                return View("SqlError");
            }
            catch (PermanentException ex)
            {
                ViewBag.Message = ex.Message;
                return View("PermanentError");
            }
        }

        /// <summary>
        /// Maakt een nieuwe instructor aan
        /// </summary>
        /// <param name="vm">De details van de instructor</param>
        /// <returns>De geüpdate index pagina met de nieuwe instructor toegevoegd</returns>
        [HttpPost]
        public IActionResult CreateInstructor(InstructorVM vm)
        {
            try
            {
                Instructor i = new Instructor(vm.Name, vm.ID, vm.Specialty);
                ic.Create(i);
                return RedirectToAction("Index");
            }
            catch (TemporaryException ex)
            {
                ViewBag.Message = ex.Message;
                return View("SqlError");
            }
            catch (PermanentException ex)
            {
                ViewBag.Message = ex.Message;
                return View("PermanentError");
            }
        }

        /// <summary>
        /// Haalt de gegevens op van de instructor die bij het ID hoort, maakt van de instructor een
        /// InstructorVM en geeft deze mee aan de view
        /// </summary>
        /// <param name="id">Het ID van de instructor</param>
        /// <returns>De UpdateInstructor view met de InstructorVM</returns>
        public IActionResult UpdateInstructor(int id)
        {
            try
            {
                if (HttpContext.Session.GetString("IsAdmin") == "True")
                {
                    Instructor i = ic.FindByID(id);
                    InstructorVM vm = new InstructorVM(i);
                    return View("UpdateInstructor", vm);
                }
                else
                {
                    return RedirectToAction("Login", "Home");
                }
            }
            catch (TemporaryException ex)
            {
                ViewBag.Message = ex.Message;
                return View("SqlError");
            }
            catch (PermanentException ex)
            {
                ViewBag.Message = ex.Message;
                return View("PermanentError");
            }
        }

        /// <summary>
        /// Update de instructor
        /// </summary>
        /// <param name="vm">De meegegeven instructor</param>
        /// <returns>de Index view</returns>
        [HttpPost]
        public IActionResult UpdateInstructor(InstructorVM vm)
        {
            try
            {
                Instructor i = new Instructor(vm.Name, vm.ID, vm.Specialty);
                ic.Update(i);
                return RedirectToAction("Index");
            }
            catch (TemporaryException ex)
            {
                ViewBag.Message = ex.Message;
                return View("SqlError");
            }
            catch (PermanentException ex)
            {
                ViewBag.Message = ex.Message;
                return View("PermanentError");
            }
        }

        /// <summary>
        /// Haalt de gegevens van de instructor op die bij het meegegeven ID hoort, maakt hier een
        /// InstructorVM van en geeft deze mee aan de view
        /// </summary>
        /// <param name="id">Het ID van de instructor</param>
        /// <returns>De DeleteInstructor view met de InstructorVM</returns>
        [HttpGet]
        public IActionResult DeleteInstructor(int id)
        {
            try
            {
                if (HttpContext.Session.GetString("IsAdmin") == "True")
                {
                    Instructor i = ic.FindByID(id);
                    InstructorVM vm = new InstructorVM(i);
                    return PartialView("DeleteInstructor", vm);
                }
                else
                {
                    return RedirectToAction("Login", "Home");
                }
            }
            catch (TemporaryException ex)
            {
                ViewBag.Message = ex.Message;
                return View("SqlError");
            }
            catch (PermanentException ex)
            {
                ViewBag.Message = ex.Message;
                return View("PermanentError");
            }
        }

        /// <summary>
        /// Delete de instructor
        /// </summary>
        /// <param name="vm">De meegegeven instructor</param>
        /// <returns>De geüpdate index view met de instructor verwijderd</returns>
        [HttpPost]
        public IActionResult DeleteInstructor(InstructorVM vm)
        {
            try
            {
                Instructor i = new(vm.Name, vm.ID, vm.Specialty);
                ic.Delete(i);
                return RedirectToAction("Index");
            }
            catch (TemporaryException ex)
            {
                ViewBag.Message = ex.Message;
                return View("SqlError");
            }
            catch (PermanentException ex)
            {
                ViewBag.Message = ex.Message;
                return View("PermanentError");
            }
        }
    }
}
