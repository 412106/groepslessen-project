﻿using BusnLogicGL;
using UTDAL;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using Semester2GroepslessenASP.Models;

namespace Semester2GroepslessenASP.Controllers
{
    public class UserController : Controller
    {
        UserContainer uc = new UserContainer(new UserMSSQLDAL());

        /// <summary>
        /// Deze view wordt aangeroepen na het inloggen. De gegevens van de user worden opgehaald,
        /// van de user wordt een UserVM gemaakt en deze worden aan de view meegegeven.
        /// </summary>
        /// <returns>De Index view met de UserVM</returns>
        [HttpGet]
        public IActionResult Index()
        {
            try
            {
                string id = HttpContext.Session.GetString("ID");
                if (id != null)
                {
                    User user = uc.FindByID(Convert.ToInt64(id));
                    UserVM vm = new UserVM(user);
                    return View(vm);
                }
                return RedirectToAction("Login", "Home");
            }
            catch (TemporaryException ex)
            {
                ViewBag.Message = ex.Message;
                return View("SqlError");
            }
            catch (PermanentException ex)
            {
                ViewBag.Message = ex.Message;
                return View("PermanentError");
            }
        }

        /// <summary>
        /// Haalt alle users op, maakt hier UserVMs van, voegt deze toe aan een lijst
        /// en stuurt dit door naar de view.
        /// </summary>
        /// <returns>De AllUsers view, waaran de users worden meegegeven</returns>
        public IActionResult AllUsers()
        {
            try
            {
                if (HttpContext.Session.GetString("IsAdmin") == "True")
                {
                    List<User> users = uc.GetAll();
                    List<UserVM> userVMs = new List<UserVM>();
                    userVMs.Clear();
                    foreach (User u in users)
                    {
                        userVMs.Add(new UserVM(u.FirstName, u.LastName, u.AddressLine1, u.AddressLine2, u.DateOfBirth, u.Email, u.Password, u.PhoneNumber, u.ID, u.IsAdmin));
                    }
                    return View(userVMs);
                }
                else
                {
                    return RedirectToAction("Login", "Home");
                }
            }
            catch (TemporaryException ex)
            {
                ViewBag.Message = ex.Message;
                return View("SqlError");
            }
            catch (PermanentException ex)
            {
                ViewBag.Message = ex.Message;
                return View("PermanentError");
            }
        }

        /// <summary>
        /// Deze methode wordt aangeroepen wanneer de user een nieuwe user wil aanmaken
        /// </summary>
        /// <returns>De CreateUser view</returns>
        [HttpGet]
        public IActionResult CreateUser()
        {
            try
            {
                if (HttpContext.Session.GetString("IsAdmin") == "True")
                {
                    return View();
                }
                else
                {
                    return RedirectToAction("Login", "Home");
                }
            }
            catch (TemporaryException ex)
            {
                ViewBag.Message = ex.Message;
                return View("SqlError");
            }
            catch (PermanentException ex)
            {
                ViewBag.Message = ex.Message;
                return View("PermanentError");
            }
        }

        /// <summary>
        /// Maakt een nieuwe user aan
        /// </summary>
        /// <param name="um">De details van de user</param>
        /// <returns>De geüpdate AllUsers pagina met de nieuwe user toegevoegd</returns>
        [HttpPost]
        public IActionResult CreateUser(UserVM um)
        {
            try
            {
                User u = new User(um.FirstName, um.LastName, um.AddressLine1, um.AddressLine2, um.DateOfBirth, um.Email, um.Password, um.PhoneNumber, um.ID, um.IsAdmin);
                uc.Create(u);
                return RedirectToAction("AllUsers");
            }
            catch (TemporaryException ex)
            {
                ViewBag.Message = ex.Message;
                return View("SqlError");
            }
            catch (PermanentException ex)
            {
                ViewBag.Message = ex.Message;
                return View("PermanentError");
            }
        }

        /// <summary>
        /// Haalt de gegevens op van de user die bij het ID hoort, maakt van de user een
        /// UserVM en geeft deze mee aan de view
        /// </summary>
        /// <param name="id">Het ID van de user</param>
        /// <returns>De UpdaetUser view met de UserVM</returns>
        [HttpGet]
        public IActionResult UpdateUser(int id)
        {
            try
            {
                if (HttpContext.Session.GetString("IsAdmin") == "True")
                {
                    User u = uc.FindByID(id);
                    UserVM vm = new UserVM(u.FirstName, u.LastName, u.AddressLine1, u.AddressLine2, u.DateOfBirth, u.Email, u.Password, u.PhoneNumber, u.ID, u.IsAdmin);
                    return View(vm);
                }
                else
                {
                    return RedirectToAction("Login", "Home");
                }
            }
            catch (TemporaryException ex)
            {
                ViewBag.Message = ex.Message;
                return View("SqlError");
            }
            catch (PermanentException ex)
            {
                ViewBag.Message = ex.Message;
                return View("PermanentError");
            }
        }

        /// <summary>
        /// Update de user
        /// </summary>
        /// <param name="vm">De meegegeven user</param>
        /// <returns>de AllUsers view</returns>
        [HttpPost]
        public IActionResult UpdateUser(UserVM vm)
        {
            try
            {
                User u = new User(vm.FirstName, vm.LastName, vm.AddressLine1, vm.AddressLine2, vm.DateOfBirth, vm.Email, vm.Password, vm.PhoneNumber, vm.ID, vm.IsAdmin);
                uc.Update(u);
                return RedirectToAction("AllUsers");
            }
            catch (TemporaryException ex)
            {
                ViewBag.Message = ex.Message;
                return View("SqlError");
            }
            catch (PermanentException ex)
            {
                ViewBag.Message = ex.Message;
                return View("PermanentError");
            }
        }

        /// <summary>
        /// Haalt de gegevens van de user op die bij het meegegeven ID hoort, maakt hier een
        /// UserVM van en geeft deze mee aan de view
        /// </summary>
        /// <param name="id">Het ID van de user</param>
        /// <returns>De DeleteUser view met de UserVM</returns>
        [HttpGet]
        public IActionResult DeleteUser(int id)
        {
            try
            {
                if (HttpContext.Session.GetString("IsAdmin") == "True")
                {
                    User u = uc.FindByID(id);
                    UserVM vm = new UserVM(u);
                    return PartialView(vm);
                }
                else
                {
                    return RedirectToAction("Login", "Home");
                }
            }
            catch (TemporaryException ex)
            {
                ViewBag.Message = ex.Message;
                return View("SqlError");
            }
            catch (PermanentException ex)
            {
                ViewBag.Message = ex.Message;
                return View("PermanentError");
            }
        }

        /// <summary>
        /// Delete de user
        /// </summary>
        /// <param name="vm">De meegegeven user</param>
        /// <returns>De geüpdate AllUsers view met de user verwijderd</returns>
        [HttpPost]
        public IActionResult DeleteUser(UserVM vm)
        {
            try
            {
                if (HttpContext.Session.GetString("IsAdmin") == "True")
                {
                    User u = new(vm.FirstName, vm.LastName, vm.AddressLine1, vm.AddressLine2, vm.DateOfBirth, vm.Email, vm.Password, vm.PhoneNumber, vm.ID, vm.IsAdmin);
                    uc.Delete(u);
                    return RedirectToAction("AllUsers");
                }
                else
                {
                    return RedirectToAction("Login", "Home");
                }
            }
            catch (TemporaryException ex)
            {
                ViewBag.Message = ex.Message;
                return View("SqlError");
            }
            catch (PermanentException ex)
            {
                ViewBag.Message = ex.Message;
                return View("PermanentError");
            }
        }
    }
}
